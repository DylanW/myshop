package com.wlm.wlm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
@Controller
public class WlmApplication {

    public static void main(String[] args) {
        SpringApplication.run(WlmApplication.class, args);
    }
    @RequestMapping("/index")
    public String index(){
//        localhost:8080/index
                return "index";
    }

    @RequestMapping("/checkout")
    public String checkout(){
//        localhost:8080/hello
        return "checkout";
    }
    @RequestMapping("/cart")
    public String cart(){
//        localhost:8080/hello
        return "cart";
    }

    @RequestMapping("/shop")
    public String shop(){
//        localhost:8080/hello
        return "shop";
    }

    @RequestMapping("/single-product")
    public String singleproduct(){
//        localhost:8080/hello
        return "single-product";
    }
}
