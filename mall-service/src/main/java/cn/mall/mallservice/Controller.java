package cn.mall.mallservice;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Auther: dylanWu
 * @Date: 2018/7/11 0011 17:02
 * @Description:
 */
@org.springframework.stereotype.Controller
public class Controller {
    @ResponseBody
    @RequestMapping("/hello")
    public String say(){
        return "hello";
    }
}
